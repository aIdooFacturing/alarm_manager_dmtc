package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";
	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
	//common func
	
	
	@Override
	public String getJigList4Report(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo >dvcList = sql.selectList(namespace + "getJigList4Report", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dvcList.size(); i++){
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dvcList.get(i).getName(), "utf-8"));
			map.put("dvcId", URLEncoder.encode(dvcList.get(i).getDvcId(), "utf-8"));
			list.add(map);
		};
		
		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);
		
		return str;
	}

	@Override
	public String getAlarmData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> alarmList = sql.selectList(namespace + "getAlarmData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < alarmList.size(); i++){
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(alarmList.get(i).getName(),"UTF-8"));
			map.put("startDateTime", alarmList.get(i).getStartDateTime());
			map.put("endDateTime", alarmList.get(i).getEndDateTime());
			map.put("delayTimeSec", alarmList.get(i).getDelayTimeSec());
			map.put("alarmCode", alarmList.get(i).getAlarmCode());
			map.put("alarmMsg",  URLEncoder.encode(alarmList.get(i).getAlarmMsg(),"UTF-8"));
			map.put("dvcId", alarmList.get(i).getDvcId());
			map.put("exist", alarmList.get(i).getExist());
			
			list.add(map);
		};
		
		Map alarmMap = new HashMap();
		alarmMap.put("alarmList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmMap);

		return str;
	}

	@Override
	public void setAlarmException(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.insert(namespace+"setAlarmException", chartVo);
	}

	@Override
	public void unsetAlarmException(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.delete(namespace+"unsetAlarmException", chartVo);
	}
};