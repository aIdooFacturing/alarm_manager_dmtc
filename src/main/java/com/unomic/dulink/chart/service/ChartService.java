package com.unomic.dulink.chart.service;

import com.unomic.dulink.chart.domain.ChartVo;


public interface ChartService {
	public String getJigList4Report(ChartVo chartVo) throws Exception;
	public String getAlarmData(ChartVo chartVo) throws Exception;
	
	//common func
	public String login(ChartVo chartVo) throws Exception;
	public String getStartTime(ChartVo chartVo) throws Exception;
	public String getComName(ChartVo chartVo) throws Exception;
	public ChartVo getBanner(ChartVo chartVo) throws Exception;
	public void setAlarmException(ChartVo chartVo) throws Exception;
	public void unsetAlarmException(ChartVo chartVo) throws Exception;
	
};
